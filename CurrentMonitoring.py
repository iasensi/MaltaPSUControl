#!/usr/bin/env python
#############################################
# MALTA PSU Current Monitoring Display
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# March 2018
#############################################

import os
import sys
import math
import array
import time
import signal
import argparse
import SerialCom
import realtime
import ROOT
import TTi
import Keithley

parser = argparse.ArgumentParser()
parser.add_argument('-o','--outdir',help='output directory',default='/tmp')
args = parser.parse_args()

ROOT.gStyle.SetPadLeftMargin(0.14)
ROOT.gStyle.SetPadBottomMargin(0.15)
ROOT.gStyle.SetPadRightMargin(0.15)
ROOT.gStyle.SetPadTopMargin(0.07)
ROOT.gStyle.SetLabelSize(0.07,"x")
ROOT.gStyle.SetLabelSize(0.07,"y")
ROOT.gStyle.SetTitleSize(0.07,"x")
ROOT.gStyle.SetTitleSize(0.07,"y")
ROOT.gStyle.SetTitleOffset(1.10,"y")
ROOT.gStyle.SetOptTitle(0);

cont = True
def signal_handler(signal, frame):
    print "You pressed ctrl+C to quit" 
    global cont
    cont = False
    return

Avdd_Dac = TTi.TTi("/dev/ttyUSB1")
Dvdd_Lvdd = TTi.TTi("/dev/ttyUSB2")
Pwell_Sub = TTi.TTi("/dev/ttyUSB3")
HV = Keithley.Keithley("/dev/ttyUSB0")
Pwell_Sub = TTi.TTi("/dev/ttyUSB3")
IMON0 = Keithley.Keithley("/dev/ttyUSB6")
IMON2 = Keithley.Keithley("/dev/ttyUSB4")

c1 = ROOT.TCanvas("c","Current Monitoring", 800,800)
c1.Divide(1,2,0.001,0.02)
gC = realtime.Plot("C",";Time [s]; Current [mA]")
gV = realtime.Plot("V",";Time [s]; Voltage [V]")
gC.SetVerbose(True)
#gC.SetRangeUser(-40,600)
#gV.SetRangeUser(-1,7)
PSUs = {Avdd_Dac:["Avdd","Dac"],Dvdd_Lvdd:["Dvdd","Lvdd"],Pwell_Sub:["Pwell","Sub"],IMON0:"IMON0",IMON2:"IMON2"}
print PSUs

count=1
for ps in PSUs:
    for output in xrange(2):
        if "IMON" in PSUs[ps] and output == 2: continue 
        if "IMON" in PSUs[ps] and output == 1: 
            print "Add plot: %s" % ps
            gC.AddPlot(PSUs[ps],"PL")
            gV.AddPlot(PSUs[ps],"PL")
            c1.Update()
            count+=1
            pass
        else:
            print "Add plot: %s" % ps
            gC.AddPlot(PSUs[ps][output],"PL")
            gV.AddPlot(PSUs[ps][output],"PL")
            c1.Update()
            count+=1
            pass
        pass
    pass
        
time0=time.time()

signal.signal(signal.SIGINT, signal_handler)
while True:
    if cont==False: break
    count=1
    for ps in PSUs:
        if cont==False: break
        for output in xrange(2):
            output+=1
            #print ps
            if ("IMON" in PSUs[ps]) and (output == 2): continue 
            if ("IMON" in PSUs[ps]) and (output == 1): 
                print "***************************"
                curr = ps.getCurrent()
                volt = ps.getVoltage()
                c1.cd(1)
                gC.AddPoint(PSUs[ps],time.time()-time0,curr)
                gC.Draw("A")
                c1.cd(2)
                gV.AddPoint(PSUs[ps],time.time()-time0,volt)
                gV.Draw("A")
                print PSUs[ps],"voltage: ",volt,"current: ",curr
                count+=1
                pass
            else:
                curr = ps.getCurrent(output)
                volt = ps.getVoltage(output)
                curr = float(curr.split("A")[0])*1000
                volt = float(volt.split("V")[0])
                c1.cd(1)
                gC.AddPoint(PSUs[ps][output-1],time.time()-time0,curr)
                gC.Draw("A")
                c1.cd(2)
                gV.AddPoint(PSUs[ps][output-1],time.time()-time0,volt)
                gV.Draw("A")
                print PSUs[ps][output-1],"voltage: ",volt,"current: ",curr
                count+=1
                pass
            print curr
            #curr = float(curr[:-1])*1000
            print volt
            c1.Modified()
            c1.Update()
            if "IMON" in PSUs[ps]: continue
            pass
        pass
    pass


'''
    print "cleaning the house"
    outimage=args.outdir+"realtime.png"
    c1.SaveAs(outimage)
    c1.SaveAs(outimage[:-3]+"root")
    ps1.setVoltage(0.0)
    ps1.setCurrent(0.0)
    ps1.enableOutput(False)
    ps1.close()
    for i in voltrons:
        voltrons[i].close()
        pass

    log = error
    if run == 0:
        log += "Full Air Measurement Complete."
        pass
    else:
        log += "No Air Measurement Complete."
        pass
    
    log += "\n"
    log += "outdir: %s\n"%args.outdir
    log += "surface: %.2f cm2\n"%args.surface
    log += "temperature: %.2f C\n"%args.temperature
    log += "delay between measurements: %i s\n"%args.delay
    log += "measurement duration: %i s\n"%args.time
    log += "airflow delay: %i s\n"%args.traci
    log += "data path: %s\n" % newpath
    
    print "Submit elog entry"
    entry = elog.elog("ade-pixel-elog",8080)
    if cont and not args.dry:
        entry.submit("SR1",{"Author":"TFM robot","Subject":"Measurement","Category":"Measurement","Text":log},
                     [outimage,])
        pass
    
    print "Send mail"
    if args.Abhi == True: to = '+41754118217@mail2sms.cern.ch'
    else: to = 'ep-ade-id-tfm@cern.ch'
    user = 'adecmos@cern.ch'
    pwd = str(base64.b64decode("S2lnZUJlbGUxOA=="))
    subject = "TFM measurement"
    sms = alert.alert(user,pwd)
    if cont or len(error)>0: sms.sendMime(to,subject,log,outimage)

    fw2.close()
    run+=1

air.enableOutput(True)

if cont:
    print "Measurement completed"
    raw_input("Type any key to continue")
    pass
'''

