enablePSU DUT1_SUB
enablePSU DUT2_SUB
enablePSU DUT_PWELL

rampVoltageUp DUT1_SUB   1.00 2.0
rampVoltageUp DUT2_SUB  1.00 2.0
rampVoltageUp DUT_PWELL 1.00 2.0

rampVoltageUp DUT1_SUB   2.00 2.0
rampVoltageUp DUT2_SUB  2.00 2.0
rampVoltageUp DUT_PWELL 2.00 2.0

rampVoltageUp DUT1_SUB   3.00 2.0
rampVoltageUp DUT2_SUB  3.00 2.0
rampVoltageUp DUT_PWELL 3.00 2.0

rampVoltageUp DUT1_SUB   4.00 2.0
rampVoltageUp DUT2_SUB  4.00 2.0
rampVoltageUp DUT_PWELL 4.00 2.0

rampVoltageUp DUT1_SUB   5.00 2.0
rampVoltageUp DUT2_SUB  5.00 2.0
rampVoltageUp DUT_PWELL 5.00 2.0

rampVoltageUp DUT1_SUB   6.00 2.0
rampVoltageUp DUT2_SUB  6.00 2.0
rampVoltageUp DUT_PWELL 6.00 2.0

setVoltage DUT_DVDD 1.80
enablePSU DUT_DVDD

setVoltage DUT_AVDD 1.80
enablePSU DUT_AVDD

setVoltage DUT_M2 1.80
enablePSU DUT_M2
