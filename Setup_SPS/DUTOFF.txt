disablePSU DUT_LVDD 
setVoltage DUT_LVDD 0.0 
 
disablePSU DUT_DVDD 
setVoltage DUT_DVDD 0.0 
 
disablePSU DUT_AVDD 
setVoltage DUT_AVDD 0.0 

rampVoltage DU1T_SUB   6.00 5.0

rampVoltage DUT_PWELL 5.50 2.0 
rampVoltage DUT1_SUB   5.50 2.0  

rampVoltage DUT_PWELL 5.00 2.0 
rampVoltage DUT1_SUB   5.00 2.0

rampVoltage DUT_PWELL 4.50 2.0 
rampVoltage DUT1_SUB   4.50 2.0 

rampVoltage DUT_PWELL 4.00 2.0 
rampVoltage DUT1_SUB   4.00 2.0 

rampVoltage DUT_PWELL 3.00 2.0 
rampVoltage DUT1_SUB   3.00 2.0 

rampVoltage DUT_PWELL 2.00 2.0 
rampVoltage DUT1_SUB   2.00 2.0 

rampVoltage DUT_PWELL 1.00 2.0 
rampVoltage DUT1_SUB   1.00 2.0 

rampVoltage DUT_PWELL 0.00 2.0 
rampVoltage DUT1_SUB   0.00 2.0  

 
disablePSU DUT_PWELL 

