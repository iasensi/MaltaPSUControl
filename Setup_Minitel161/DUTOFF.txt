disablePSU DUT_DVDD
setVoltage DUT_DVDD 0.0

disablePSU DUT_AVDD
setVoltage DUT_AVDD 0.0

rampVoltage DUT_PWELL 5.50 1.0
rampVoltage DUT_SUB   5.50 1.0

rampVoltage DUT_PWELL 5.00 1.0
rampVoltage DUT_SUB   5.00 1.0

rampVoltage DUT_PWELL 4.00 1.0
rampVoltage DUT_SUB   4.00 1.0

rampVoltage DUT_PWELL 3.00 1.0
rampVoltage DUT_SUB   3.00 1.0

rampVoltage DUT_PWELL 2.00 1.0
rampVoltage DUT_SUB   2.00 1.0

rampVoltage DUT_PWELL 1.00 1.0
rampVoltage DUT_SUB   1.00 1.0

rampVoltage DUT_PWELL 0.00 1.0
rampVoltage DUT_SUB   0.00 1.0

disablePSU DUT_PWELL
