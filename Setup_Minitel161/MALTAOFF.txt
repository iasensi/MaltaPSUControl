disablePSU LVDD
setVoltage LVDD 0.0

disablePSU DVDD
setVoltage DVDD 0.0

disablePSU AVDD
setVoltage AVDD 0.0

rampVoltage PWELL 5.50 1.0
rampVoltage SUB   5.50 1.0

rampVoltage PWELL 5.00 1.0
rampVoltage SUB   5.00 1.0

rampVoltage PWELL 4.00 1.0
rampVoltage SUB   4.00 1.0

rampVoltage PWELL 3.00 1.0
rampVoltage SUB   3.00 1.0

rampVoltage PWELL 2.00 1.0
rampVoltage SUB   2.00 1.0

rampVoltage PWELL 1.00 1.0
rampVoltage SUB   1.00 1.0

rampVoltage PWELL 0.00 1.0
rampVoltage SUB   0.00 1.0

disablePSU PWELL

