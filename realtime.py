#!/usr/bin/env python
##################################################
# 
# Realtime plots for monitoring applications
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# May 2016
#
##################################################

import ROOT

class Plot:
    def __init__(self):
        self.build()
        pass
    def __init__(self,name):
        self.build()
        self.name = name
        pass
    def __init__(self,name,title):
        self.build()
        self.name = name
        self.title = title
        pass
    def build(self):
        self.name=None
        self.title=None
        self.gv={}
        self.opts={}
        self.leg=ROOT.TLegend(0.86,0.20,0.99,0.90)
        self.drawn=False
        self.ymin=1000000
        self.ymax=-1000000
        self.xmin=0
        self.xmax=0
        self.forcerange=False
        self.verbose=False
        pass
    def SetVerbose(self,enable):
        self.verbose=enable
        pass
    def SetTitle(title):
        self.title=title
        pass
    def AddPlot(self,opts):
        g = ROOT.TGraph(0)
        i = len(self.gv)
        g.SetName("g%s%i"%(self.name,i))
        if self.title: g.SetTitle(self.title)
        g.SetMarkerColor(i+1)
        g.SetLineColor(i+1)
        self.leg.AddEntry(g,"%s %i"%(self.name,i+1),"lp")
        self.gv[i]=g
        self.opts[i]=opts
        pass
    def AddPlot(self,key,opts):
        g = ROOT.TGraph(0)
        g.SetName("g%s%s"%(self.name,key))
        if self.title: g.SetTitle(self.title)
        i = len(self.gv)
        g.SetMarkerColor(i+1)
        g.SetLineColor(i+1)
        self.leg.AddEntry(g,"%s %s"%(self.name,key),"lp")
        self.gv[key]=g
        self.opts[key]=opts
        pass
    def SetPlot(self,g,opts):
        found=False
        for i in self.gv:
            if self.gv[i]==g: found=True
            pass
        if found==True: return
        i=len(self.gv)
        self.gv[i]=g
        self.opts[i]=opts
        pass
    def AddPoint(self,key,x,y):
        if not key in self.gv:
            print "Realtime::AddPoint Plot not found with key:",key
            return
        self.gv[key].SetPoint(self.gv[key].GetN(),x,y)
        if y>self.ymax and self.forcerange==False: self.ymax=y+0.1*abs(y)
        if y<self.ymin and self.forcerange==False: self.ymin=y-0.1*abs(y)        
        if x>self.xmax: self.xmax=x
        if x<self.xmin: self.xmin=x        
    def SetRangeUser(self,ymin,ymax):
        self.forcerange = True
        self.ymin = ymin
        self.ymax = ymax
    def GetYmin(self):
        return self.ymin
    def GetYmax(self):
        return self.ymax
    def GetXmin(self):
        return self.xmin
    def GetXmax(self):
        return self.xmax
    def SetLegend(self,leg):
        self.leg = leg
    def Draw(self,opts):
        for key in self.gv.iterkeys():
            if self.gv[key].GetN()==0:
                if self.verbose: print "Realtime::Draw Dont draw"
                return
        fkey=self.gv.keys()[0]
        if self.drawn:
            if self.verbose: print "Realtime::Draw Set y range = %.2f,%.2f" % (self.ymin,self.ymax)
            self.gv[fkey].GetYaxis().SetRangeUser(self.ymin,self.ymax)
            return
        if self.verbose: print "Realtime::Draw ===========> Draw NOW <============="
        self.ForceDraw(opts)
        pass
    def ForceDraw(self,opts=""):
        fkey=self.gv.keys()[0]
        for key in self.gv.iterkeys():
            if self.title:
                if key==fkey: self.gv[key].Draw("A%s"%self.opts[key])
                else: self.gv[key].Draw("SAME%s"%self.opts[key])
        if self.leg!=None:
            if self.verbose: print "Realtime::ForceDraw Draw the legend"
            self.leg.Draw()
        self.drawn = True
        ROOT.gSystem.ProcessEvents()
        pass
    pass
