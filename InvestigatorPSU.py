#!/usr/bin/env python
import TTi
import Keithley
import time 
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-on",help="Power Investigator ON",action='store_true')
parser.add_argument("-off",help="Power Investigator OFF",action='store_true')

args=parser.parse_args()


Amp_Rst        = TTi.TTi("/dev/ttyUSB0")
TelVlow_Hitbus = TTi.TTi("/dev/ttyUSB1")
Vplus          = TTi.TTi("/dev/ttyUSB2")
TelHV          = Keithley.Keithley("/dev/ttyUSB3")
Pwell          = Keithley.Keithley("/dev/ttyUSB4")
Sub            = Keithley.Keithley("/dev/ttyUSB5")


if args.on:
    print "Turn on Investigator"
    Amp_Rst.setVoltageLimit(1,12)
    Amp_Rst.setCurrentLimit(1,0.2)
    Amp_Rst.setVoltage(1,12)
    Amp_Rst.enableOutput(1,True)

    time.sleep(1)

    Sub.setVoltageLimit(1)
    Sub.setCurrentLimit(0.001)
    Sub.setVoltage(0)
    Sub.enableOutput(True)

    time.sleep(1)

    Pwell.setVoltageLimit(0)
    Pwell.setCurrentLimit(0.001)
    Pwell.setVoltage(0)
    Pwell.enableOutput(True)

    time.sleep(1)

    Vplus.setVoltageLimit(1,1.8)
    Vplus.setCurrentLimit(1,0.040)
    Vplus.setVoltage(1,1.8)
    Vplus.enableOutput(1,True)

    time.sleep(1)

    Amp_Rst.setVoltageLimit(2,6.8)
    Amp_Rst.setCurrentLimit(2,0.010)
    Amp_Rst.setVoltage(2,6.67)
    Amp_Rst.enableOutput(2,True)

    time.sleep(1)

    Sub.setVoltageLimit(-6)
    Sub.setCurrentLimit(0.0002)
    Sub.rampVoltage(-6,1,1)
    Sub.enableOutput(True)

    time.sleep(1)

    Pwell.setVoltageLimit(-6)
    Pwell.setCurrentLimit(0.0002)
    Pwell.rampVoltage(-6,1,1)
    Pwell.enableOutput(True)
    pass

if args.off:
    print "Turn off Investigator"
    Pwell.rampVoltage(0,1,1)
    Pwell.enableOutput(False)

    Sub.rampVoltage(0,1,1)
    Sub.enableOutput(False)

    Vplus.setVoltage(1,1.8)
    Vplus.enableOutput(1,False)

    Amp_Rst.setVoltage(2,0)
    Amp_Rst.enableOutput(2,False)

    Amp_Rst.setVoltage(1,0)
    Amp_Rst.enableOutput(1,False)

    Sub.close()
    Pwell.close()
    Vplus.close()
    Amp_Rst.close()

#TElHV : -60V 
#Tel VLOW: 2.5V 3A
#Hitbus: 1.5V 1.3A

