enablePSU SUB
enablePSU PWELL

rampVoltageUp SUB   1.00 1.0
rampVoltageUp PWELL 1.00 1.0

rampVoltageUp SUB   2.00 1.0
rampVoltageUp PWELL 2.00 1.0

rampVoltageUp SUB   3.00 1.0
rampVoltageUp PWELL 3.00 1.0

rampVoltageUp SUB   4.00 1.0
rampVoltageUp PWELL 4.00 1.0

rampVoltageUp SUB   5.00 1.0
rampVoltageUp PWELL 5.00 1.0

rampVoltageUp SUB   6.00 1.0
rampVoltageUp PWELL 6.00 1.0

########rampVoltageDown SUB   6.00 1.0

setVoltage DVDD 1.20
enablePSU DVDD

disablePSU AVDD
setVoltage AVDD 1.80
