disablePSU IMON2 


setVoltage LVDD 0.0
disablePSU LVDD

setVoltage DVDD 0.0
disablePSU DVDD

setVoltage AVDD 0.0
disablePSU AVDD

setVoltage PWELL 1.00 
setVoltage SUB  1.00 


setVoltage PWELL 0.00 
setVoltage SUB  0.00 
