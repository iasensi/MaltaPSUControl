setVoltage SUB 0.0
enablePSU SUB

setVoltage PWELL 0.0
enablePSU PWELL

setVoltage DVDD 1.80
enablePSU DVDD

setVoltage AVDD 1.80
enablePSU AVDD

rampVoltage SUB   2.0 1
rampVoltage PWELL 2.0 1

rampVoltage SUB   6.0 1
