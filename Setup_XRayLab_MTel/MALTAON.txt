enablePSU SUB
rampVoltage SUB   6.00 2.0

setVoltage DVDD   1.20
enablePSU  DVDD

setVoltage REST   1.80
disablePSU REST

setVoltage IMON2  1.10
disablePSU IMON2

