#!/usr/bin/env python
#############################################
# MALTA PSU Current Monitoring Display
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# March 2018
#############################################

import os
import sys
import math
import array
import time
import signal
import argparse
import SerialCom
import ROOT
import TTi
import Keithley
import base64
import alert
   
to = '+41754113820@mail2sms.cern.ch'
#else: to = 'ep-ade-id-tfm@cern.ch'
user = 'adecmos@cern.ch'
#pwd = str(base64.b64decode("S2lnZUJlbGUyMQ=="))
pwd = base64.b64decode(b'S2lnZUJlbGUyMQ==').decode('ascii')
sms = alert.alert(user,pwd)

subject = "cazzo"
msg="currentandrea"
sms.sendMime(to,subject,msg)
