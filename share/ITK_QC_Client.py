#!/usr/bin/env python
# client code
# Ignacio.Asensi@cern.ch

import os
import sys
import json
import socket 


class SocketClient():
    
    def __init__(self):
        print("SocketClient version 1.0")
        self.verbose=False
        self.host=""
        self.port=9998
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(3)
        self.connected=False
        pass
    
    def setVerbose(self, value):
        self.verbose=value
        pass

    def connect(self, host, port=9998):
        self.host=host
        self.port=port
        print("Connecting to server: %s:%i" % (self.host, self.port))
        try:
            self.s.connect((self.host, self.port))
            self.connected=True
            return True
        except:
            print("Could not connect to server")
            self.connected=False
            return False
        pass

    def close(self):
        self.s.close()
        pass

    def log(msg):
        print("Server: %s %s" % (timestamp(),msg))
        pass

    def timestamp():
        return time.strftime("%Y-%m-%d %H:%M:%S")
  
    def sendCmd(self, cmd, data=""):
        #parse arguments to send with the command
        req={"cmd":cmd, "data":data}
        s_req = json.dumps(req)
        n_req = "%08x" % len(s_req)
        if self.verbose: log("Send len: %s" % n_req)
        self.s.send(n_req.encode())
        if self.verbose: log("Send msg: %s" % s_req)
        self.s.send(s_req.encode())
        if self.verbose: log("Wait for reply...")
        n_rep = int(self.s.recv(8),16)
        if self.verbose: log("Recv len: %i" % n_rep)
        s_rep = self.s.recv(n_rep)
        if self.verbose: log("Recv msg: %s" % s_rep)
        if self.verbose: log("Decode message")
        rep=json.loads(s_rep)
        if self.verbose: log("Close connection")
        #self.close()
        if "OK" in rep["con"]:
            return rep
        else:
            log("Reply not OK:" % rep)
            return False
        pass
    pass

if __name__ == "__main__":
    
    client=SocketClient()
    client.connect(socket.gethostname())
    client.sendCmd("Ping","data test")
    client.close()
    print("done")
        
    
