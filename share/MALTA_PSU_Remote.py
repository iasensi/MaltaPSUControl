#!/usr/bin/env python
# ############################################
# remote interface which uses the server
# ignacio.asensi@cern.ch

import os
import sys
import MaltaPSUClient
import argparse
import socket

if __name__ == '__main__':
    
    hostname=socket.gethostname()
    parser = argparse.ArgumentParser()
    parser.add_argument("-s","--server",help="Server", default=hostname)
    parser.add_argument("-c","--command",help="Command", metavar=('cmd','psu','val'), nargs=3)
    parser.add_argument("-t","--task",help="Task")
    parser.add_argument("-v","--verbose",help="Enable verbose mode",action='store_true')
    
    args=parser.parse_args()
      
    print("This is MALTA_PSU_Remote")
    psu=MaltaPSUClient.MaltaPSUClient()
    if args.verbose: psu.setVerbose(args.verbose)

    if args.verbose: print("Check connection")
    works= psu.connect(args.server)
    if not works:
        print "Cannot connect"
        sys.exit()
    
    if args.task:
        if args.task == "ping":
            print psu.checkConnection()
            pass
        elif args.task.endswith(".txt"):
            task=os.environ["MALTAPSU_SETUP"]+"/"+args.task
            if not os.path.exists(task):
                print "Cannot find task file: %s" % args.task
                sys.exit()
            psu.executeTaskList(task)
            pass
        pass
    elif args.command:
        ret=psu.sendCmd(args.command[0],args.command[1],args.command[2])
        if ret==False: print "Error executing command"
        print (ret["Reply"])
        pass
    else:
        print "Task not defined"
        pass
    
    print("Have a nice day")

