#!/usr/bin/env python
import io
import serial
from SerialCom import SerialCom
import re
import TTi_Glasgow
import TTiSingle
import time
import sys
import alert
import os
import datetime
import base64
import argparse

#define class...
class MALTA_PSU:
    def __init__(self, sysArgs):
        self.PSUNames = []
        self.addedPSUs = dict()
        self.PSUtypes = dict()
        self.PSUchannel = dict()
        self.PSUPort = dict()
        self.PSUVoltageLimit = dict()
        self.PSUCurrentLimit = dict()
        self.mailSender = None
        self.mailSenderPW = None
        self.mailRecipients = []
        
        self.executeTaskList(os.environ["MALTAPSU_SETUP"]+"/setUpPSU.txt")

        taskList = ""
        commands = []
        args = []
        cmdArgs = []

        argType = ""
        for i in range(1,len(sysArgs)):
            if sysArgs[i] == "-c":
                if argType == "arg":
                    args.append(cmdArgs)
                    cmdArgs = []
                argType = sysArgs[i]
                continue
            if sysArgs[i] == "-t":
                if argType == "arg":
                    args.append(cmdArgs)
                    cmdArgs = []
                argType = sysArgs[i]
                continue
            if argType == "-t":
                taskList = sysArgs[i]
                continue
            if argType ==  "-c":
                commands.append(sysArgs[i])
                argType = "arg"
                continue
            if argType == "arg":
                cmdArgs.append(sysArgs[i])
                continue
        args.append(cmdArgs)
        if taskList != "":
            print "ABOUT TO EXECUTE TASKLIST: "+os.environ["MALTAPSU_SETUP"]+"/"+taskList
            self.executeTaskList(os.environ["MALTAPSU_SETUP"]+"/"+taskList)
        if commands != [] and args != []:
            if len(commands) is not len(args):
                print "In __init__: command and arg vectors have different size!"
            for i in range(0,len(commands)):
                self.executeCommand(commands[i],args[i])

    def addPSU(self, name = None, serialPath = None, deviceType = None, channel = 0, vLimit = 0.0, cLimit = 0.0):
        if name == None:
            name = raw_input("enter channel name (type 'c' to quit):\n")
            if name == "c":
                sys.exit("script stopped by user")
        if serialPath == None:
            serialPath = raw_input("enter port path (type 'c' to quit):\n")
            if serialPath == "c":
                sys.exit("script stopped by user")
        if deviceType == None:
            deviceType = raw_input("single or multi channel PSU? (s/m; 'c' to quit):\n")
            while deviceType != "s" and deviceType != "m" and deviceType != 'c':
                print("faulty command!")
                deviceType = raw_input("single or multi channel PSU? (s/m; 'c' to quit):\n")
        com = None
        if deviceType == "c":
            sys.exit("script stopped by user")
        elif deviceType == "s":
            com = TTiSingle.TTiSingle(serialPath)
            channel = 1;
            if vLimit == 0.0:
                vLimit = raw_input("enter voltage limit:\n")
            if cLimit == 0.0:
                cLimit = raw_input("enter current limit:\n")
            com.setVoltageLimit(float(vLimit))
            time.sleep(0.1)
            com.setCurrentLimit(float(cLimit))
        else:
            com = TTi_Glasgow.TTi_Glasgow(serialPath)
            if channel == 0:
                channel = raw_input("which channel? (1/2):\n")
                while channel != 1 and channel != 2:
                    print("faulty input!")
                    channel = raw_input("which channel? (1/2):\n")
            if vLimit == 0.0:
                vLimit = raw_input("enter voltage limit:\n")
            if cLimit == 0.0:
                cLimit = raw_input("enter current limit:\n")
            com.setVoltageLimit(int(channel),float(vLimit))
            time.sleep(0.1)
            com.setCurrentLimit(int(channel),float(cLimit))

        self.PSUNames.append(name)
        self.addedPSUs[name] = com
        self.PSUtypes[name] = deviceType
        self.PSUchannel[name] = int(channel)
        self.PSUPort[name] = serialPath
        self.PSUVoltageLimit[name] = float(vLimit)
        self.PSUCurrentLimit[name] = float(cLimit)

        #print "Added PSU with name '{}' at port '{}' and device type '{}' at channel '{}' with vLimit= '{}' and cLimit '{}'.\n".format(name,serialPath,deviceType,channel,vLimit,cLimit)

    def enablePSU(self,PSUs):
        for i in range(0,len(PSUs)):
            if PSUs[i] in self.PSUNames:
                if self.PSUtypes[PSUs[i]] == "s":
                    self.addedPSUs[PSUs[i]].enableOutput(True)
                    print "Enabled {}".format(PSUs[i])
                elif self.PSUtypes[PSUs[i]] == "m":
                    self.addedPSUs[PSUs[i]].enableOutput(self.PSUchannel[PSUs[i]],True)
                    print "Enabled {}".format(PSUs[i])
            else:
                print "In enablePSU: Could not find {} among added PSUs!".format(PSUs[i])
                return -1

    def disablePSU(self,PSUs):
        for i in range(0,len(PSUs)):
            if PSUs[i] in self.PSUNames:
                if self.PSUtypes[PSUs[i]] == "s":
                    self.addedPSUs[PSUs[i]].enableOutput(False)
                    print "Disabled {}".format(PSUs[i])
                elif self.PSUtypes[PSUs[i]] == "m":
                    self.addedPSUs[PSUs[i]].enableOutput(self.PSUchannel[PSUs[i]],False)
                    print "Disabled {}".format(PSUs[i])
            else:
                print "In disablePSU: Could not find {} among added PSUs!".format(PSUs[i])
                return -1

    def executeTaskList(self,filepath):
        command = None
        commandArgs = []
        with open(filepath) as f:
            for line in f:
                if line[0] != '#':
                    #the following regex first checks if there are quotation marks
                    #if yes, match everything between the quotation marks, if no, match char-arrays which are delimited by whitespace
                    regex = r'((?<=\")[\w\s]+?(?=\"))|((?!\")[^\s]+)'
                    args = re.findall(regex,line)
                    for i in range(0,len(args)):
                        #regex returns a list of pairs where the first entry is only filled if the args was in quotation marks
                        #and the second entry is only filled if the entry was not, this is mutually exclusive and can be flattened
                        #by joining the pairs
                        args[i] = "".join(args[i])
                    if len(args) > 0:
                        command = args[0]
                        commandArgs = []
                        for i in range(1,len(args)):
                            commandArgs.append(args[i])
                        self.executeCommand(command,commandArgs)
                    else:
                        pass
                        #print "In executeTaskList: No arguments given, emtpy line?"
                else:
                    if line[0] == '#':
                        pass
                    else:
                        print "In executeTaskList: Could not parse line: \n {}".format(line)
        return 0

    def executeCommand(self,command,args):
        if command == 'getVoltage' and len(args) is 2:
            self.getVoltage(args[0],bool(args[1]))
        elif command == 'getVoltageLimit' and len(args) is 1:
            self.getVoltageLimit(args[0])
        elif command == 'getCurrent' and len(args) is 2:
            self.getCurrent(args[0],bool(args[1]))
        elif command == 'getCurrentLimit' and len(args) is 1:
            self.getCurrentLimit(args[0])
        elif command == 'setVoltage' and len(args) is 2:
            self.setVoltage(args[0],args[1])
        elif command == 'setVoltageLimit' and len(args) is 2:
            self.setVoltageLimit(args[0],args[1])
        elif command == 'setCurrentLimit' and len(args) is 2:
            self.setCurrentLimit(args[0],args[1])
        elif command == 'rampVoltage' and len(args) > 2:
            PSUs = []
            values = []
            for i in range(0,len(args)/2):
                PSUs.append(args[i])
            for i in range(len(args)/2,len(args)-1):
                values.append(float(args[i]))
            timeWindow = float(args[-1])
            self.rampVoltage(PSUs,values,timeWindow)
        elif command == 'showConnectedPSUs' and len(args) is 0:
            self.showConnectedPSUs()
        elif command == 'addPSU' and len(args) is 6:
            self.addPSU(args[0],args[1],args[2],args[3],args[4],args[5])
        elif command == 'executeTaskList' and len(args) is 1:
            self.executeTaskList(args[0])
        elif command == 'enablePSU' and len(args) > 0:
            self.enablePSU(args)
        elif command == 'disablePSU' and len(args) > 0:
            self.disablePSU(args)
        elif command == 'sendMail' and len(args) > 1:
            files = []
            for i in range(2,len(args)):
                files.append(args[i])
            self.sendMail(args[0],args[1],files)
        elif command == 'setupMail' and len(args) > 2:
            recipients = []
            for i in range(2,len(args)):
                recipients.append(args[i])
            self.setupMail(args[0],args[1],recipients)
        elif command == 'monitorValues' and len(args) > 2:
	    if args[0] == 'all':
		alertAction = args[1]
		complianceLimit = args[2]
		PSUs = []
		values = []
		for i in range(0,len(self.PSUNames)):
			values.append("voltage")
			values.append("current")
			PSUs.append(self.PSUNames[i])
			PSUs.append(self.PSUNames[i])
		self.monitorValues(PSUs,values,alertAction,complianceLimit)
		return 0
            PSUs = []
            values = []
            for i in range(0,len(args)/2-1):
                PSUs.append(args[i])
            for i in range(len(args)/2-1,len(args)-2):
                values.append(args[i])
            alertAction = args[-2]
            complianceLimit = int(args[-1])
            self.monitorValues(PSUs,values,alertAction,complianceLimit)
        elif command == 'sleep' and len(args) == 1:
            self.sleep(args[0])
        else:
            print "Could not parse command, this was read in:\n"
            print "Command: {}\n".format(command)
            print "Arguments: \n"
            for i in range(0,len(args)):
                print "    {}\n".format(args[i])
            return -1
        return 0

    def sleep(self,timeWindow):
        print "Sleeping for {} seconds...".format(timeWindow)
        time.sleep(float(timeWindow))

    def getVoltage(self,PSUName,printout):
        time.sleep(0.3)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's':
		try:
                    val_array = re.findall("[-]*\d*\.\d+|\d+",com.getVoltage())
		    val = float(val_array[0])
		except KeyboardInterrupt:
		    return -2
		except:
                    print "getVoltage: cannot convert: %s" % com.getVoltage(channel)
                    val=0
                    return -1
                if printout:
                    print "{}: {}V".format(PSUName,val)
                return val
            elif devType == 'm':
                try:
                    val_array = re.findall("[-]*\d*\.\d+|\d+",com.getVoltage(channel))
		    val = float(val_array[0])
		except KeyboardInterrupt:
		    return -2
                except:
                    print "getVoltage: cannot convert: %s" % com.getVoltage(channel)
                    val=0
                    return -1
                if printout:
                    print "{}: {}V".format(PSUName,val)
                return val
        else:
            print "getVoltage: Cannot find {} among added PSUs!".format(PSUName)
            return -1
        return 0

    def getCurrent(self,PSUName,printout):
        time.sleep(0.3)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's':
		try:
                    val_array = re.findall("[-]*\d*\.\d+|\d+",com.getCurrent())
		    val = float(val_array[0])
		except KeyboardInterrupt:
		    return -2
		except:
                    print "getCurrent: cannot convert: %s" % com.getCurrent(channel)
                    val=0
                    return -1
                if printout:
		    print com.getCurrent()
                    print "{}: {}A".format(PSUName,val)		    
                return val
            elif devType == 'm':
                try:
                    val_array = re.findall("[-]*\d*\.\d+|\d+",com.getCurrent(channel))
		    val = float(val_array[0])
		except KeyboardInterrupt:
		    return -2
                except:
                    print "getCurrent: cannot convert: %s" % com.getCurrent(channel)
                    val=0
                    return -1
                if printout:
                    print "{}: {}A".format(PSUName,val)
                return val
        else:
            print "getCurrent: Cannot find {} among added PSUs!".format(PSUName)
            return -1
        return 0

    def setVoltage(self,PSUName,value):
        time.sleep(0.2)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            vLimit = self.PSUVoltageLimit[PSUName]
            if abs(float(value)) > abs(float(vLimit)):
                print "Voltage exceeds voltage limit!"
                return -1
            if devType == 's':
                com.setVoltage(float(value))
            elif devType == 'm':
                com.setVoltage(int(channel),float(value))
            print "Voltage of {} set to {}".format(PSUName,value)
        else:
            print "In setVoltage: Cannot find {} among added PSUs!".format(PSUName)
            return -1
        return 0

    def setVoltageLimit(self,PSUName,value):
        time.sleep(0.2)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's':
                com.setVoltageLimit(float(value))
            elif devType == 'm':
                com.setVoltageLimit(int(channel),float(value))
            print "Voltage limit for {} set to {}".format(PSUName,value)
        else:
            print "In setVoltageLimit: Cannot find {} among added PSUs!".format(PSUName)
            return -1
        return 0

    def getVoltageLimit(self,PSUName):
        time.sleep(0.2)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's':
                val = round(float(com.getVoltageLimit()[0:-1]),2)
		print "Voltage limit for {}: {}V".format(PSUName,val)
                return val
            elif devType == 'm':
                val = round(float(com.getVoltageLimit(channel)[0:-1]),2)
		print "Voltage limit for {}: {}V".format(PSUName,val)
                return val
        else:
            print "In getVoltageLimit: Cannot find {} among added PSUs!".format(PSUName)
            return -1
        return 0

    def getCurrentLimit(self,PSUName):
        time.sleep(0.2)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's':
                val = round(float(com.getCurrentLimit()[0:-1]),2)
		print "Current limit for {}: {}A".format(PSUName,val)
                return val
            elif devType == 'm':
                val = round(float(com.getCurrentLimit(channel)[3:]),2)
		print "Current limit for {}: {}A".format(PSUName,val)
                return val
        else:
            print "In getCurrentLimit: Cannot find {} among added PSUs!".format(PSUName)
            return -1
        return 0

    def setCurrentLimit(self,PSUName,value):
        time.sleep(0.2)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's':
                com.setCurrentLimit(float(value))
            elif devType == 'm':
                com.setCurrentLimit(channel,float(value))
            print "Current limit for {} set to {}".format(PSUName,value)
        else:
            print "In setCurrentLimit: Cannot find {} among added PSUs!".format(PSUName)
            return -1
        return 0

    def setupMail(self,sender,senderPW,recipients):
        self.mailSender = sender
	senderPW = base64.b64decode(senderPW)
        self.mailSenderPW = senderPW
        for i in range(0,len(recipients)):
            self.mailRecipients.append(recipients[i])
        #####print "Mail contacts set up!"

    def sendMail(self,subject,message,files=None):
        if self.mailSender == None:
            print "Mail contacts were not set up!"
            return -1
        mailAlert = alert.alert(self.mailSender,self.mailSenderPW)
        for i in range(0,len(self.mailRecipients)):
            mailAlert.sendMime2(self.mailRecipients[i],subject,message,files)
            print "alert mail was sent to {}\n".format(self.mailRecipients[i])

    def rampVoltage(self,PSUNames,vals,tW):
        time.sleep(0.2)
        if len(PSUNames) != len(vals):
            print "In rampVoltage: Different number of PSUs and goal voltages!"
            return -1
        for i in range(0,len(PSUNames)):
            if PSUNames[i] not in self.PSUNames:
                print "In rampVoltage: Cannot find {} among added PSUs!".format(PSUName)
                return -1
            vLimit = self.PSUVoltageLimit[PSUNames[i]]
            value = vals[i]
            if abs(value) > abs(vLimit):
                print "In rampVoltage: Voltage exceeds voltage limit!"
                return -1
        voltageSteps = []
        oldVoltages =[]
        timeWindow = float(tW)
        nSteps = int(timeWindow/0.2)
        if nSteps == 0:
            nSteps = 1
        for i in range(0,len(PSUNames)):
            PSUName = PSUNames[i]
            val = vals[i]
            value = float(val)
            oldVoltage = self.getVoltage(PSUName,0)
            oldVoltages.append(oldVoltage)
            voltageChange = value-oldVoltage
            voltageStep = voltageChange/nSteps
            voltageSteps.append(voltageStep)
        for i in range(1,nSteps+1):
            for j in range(0,len(PSUNames)):
                if voltageSteps[j] <= 0.01:
                    pass
                PSUName = PSUNames[j]
                val = vals[j]
                value = float(val)
                voltageStep = voltageSteps[j]
                oldVoltage = oldVoltages[j]
                self.setVoltage(PSUName,oldVoltage+voltageStep*i)
                #some sleep is necessary to let the PSU stabilize
            time.sleep(0.2)
        for j in range(0,len(PSUNames)):
            PSUName = PSUNames[j]
            newVoltage = self.getVoltage(PSUName,0)
            print "Voltage of {} ramped from {} to {}".format(PSUName,oldVoltages[j],newVoltage)
        return 0

    def monitorValues(self,PSUs,values,alertAction,complianceLimit):
        if len(PSUs) != len(values):
            print "In monitorValues: Argument lists have different lengths!"
            return -1
        for i in range(0,len(PSUs)):
            if PSUs[i] not in self.PSUNames:
                print "In monitorValue: Cannot find {} among added PSUs!".format(PSUs[i])
                return -1
            else:
                pass
        for i in range(0,len(values)):
            if values[i] != "voltage" and values[i] != "current":
                print "In monitorValue: Cannot monitor {}, value unknown!".format(values[i])
                return -1
            else:
                pass
        #open file
        #write header line with columns for monitored PSUs
        #go through PSUs and read out the corresponding values
        #concatenate string of read values and time stamp and write to file
        #check if any value is in compliance, if yes, send alert and end monitoring if stopIfAlert=True
        if not os.path.exists("monitoring"):
            try:
                os.makedirs("monitoring")
            except OSError as e:
                if e.errno != errno.EEXIST:
                    pass
                else:
                    print "In monitorValues: Could not create monitoring Folder!"
                    return -1
        else:
            pass

        if self.mailSender == None:
            print "The mail contacts do not seem to be set up!"
            print "Please do this before starting monitoring."
            return -1

        dateandtime = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
        fileName = "monitoring/PSUmonitoring_" + dateandtime + ".txt"
        fileTwinCounter = 0
        f = None
        fileOpenFail = 0
        while(fileOpenFail < 10):
            try:
                f = open(fileName,"w")
                break;
            except:
                fileTwinCounter = fileTwinCounter+1
                fileName = "monitoring/PSUmonitoring_" + dateandtime + "_" + string(fileTwinCounter) + ".txt"
                fileOpenFail += 1

        columnWidths = []
        dateandtime2 = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        columnWidths.append(len(dateandtime2)+3)
        headerLine = "{col:<{width}}".format(width=len(dateandtime2)+3,col="#Time:")
        for i in range(0,len(PSUs)):
            entry = PSUs[i]+"("+values[i][0:1]+"):"
            columnWidths.append(len(entry)+3)
            headerLine = headerLine + "{col:<{width}}".format(width=len(entry)+3,col=entry)
        f.write(headerLine+"\n")
        complianceCounter = 0
        sentAlert = False
        keepMonitoring = True
        try:
            print "Monitoring values, press Ctrl-C to stop..."
            print headerLine
            headerLinePrint = 0
            while keepMonitoring:
                inCompliance = False
                dateandtime2 = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
                newLine = "{col:<{width}}".format(width=columnWidths[0],col=dateandtime2)
                curVal = 0.0
                for i in range(0,len(PSUs)):
                    #read in val from PSUs
                    if values[i] == "voltage":
			curVal = self.getVoltage(PSUs[i],0)
			if curVal == -2:
			    keepMonitoring = False
			    break
			if curVal == -1:
			    print "Could not read out {} from {}!".format(values[i],PSUs[i])
			    keepMonitoring = False
			    break
                        if abs(curVal) >= self.PSUVoltageLimit[PSUs[i]]:
                            inCompliance = True
                    elif values[i] == "current":
			curVal = self.getCurrent(PSUs[i],0)
			if curVal == -2:
			    keepMonitoring = False
			    break
			if curVal == -1:
			    print "Could not read out {} from {}!".format(values[i],PSUs[i])
			    keepMonitoring = False
			    break
                        if abs(curVal) >= self.PSUCurrentLimit[PSUs[i]]:
                            inCompliance = True
                    #add val to newline
                    newLine = newLine + "{col:<{width}}".format(width=columnWidths[i+1],col=curVal)
                #write newLine to file
		if not keepMonitoring:
		    break
                f.write(newLine+"\n")
                headerLinePrint += 1
                if headerLinePrint == 20:
                    print headerLine
                    headerLinePrint = 0
                print newLine
                newLine = ""
                #if limit reached, break
                if inCompliance and not sentAlert:
                    #send alert mail
                    complianceCounter += 1
                    print "ComplianceCounter: {}".format(complianceCounter)
                    if complianceCounter > complianceLimit:
                        subject = "MALTA_PSU: Monitored value in compliance!"
                        message = ""
                        files = []
                        files.append(fileName)
                        f.close()
                        self.sendMail(subject,message,files)
                        sentAlert = True
                        f = open(fileName,"a")
                        if alertAction != "None":
                            self.executeTaskList(alertAction)
                            break
                    else:
                        pass
                else:
                    complianceCounter = 0
                    pass
	    print "\n"
            print "Monitoring stopped, closing file..."
            f.close()
            print "...done, good bye."
        except KeyboardInterrupt:
	    print "\n"
            print "Monitoring stopped, closing file..."
            f.close()
            print "...done, good bye."
        f.close()
        return 0

    def showConnectedPSUs(self):
        for i in range(0,len(self.PSUNames)):
            name = self.PSUNames[i]
            channel = self.PSUchannel[name]
            port = self.PSUPort[name]
            print "PSU {} on channel {} of port {}\n".format(name,channel,port)
        if len(self.PSUNames) is 0:
            print "No PSUs seem to be connected!"
        return 0

#class is defined, now execute tasks
psu = MALTA_PSU(sys.argv)
