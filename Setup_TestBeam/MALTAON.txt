#executeTaskList /home/sbmuser/MaltaSW/MaltaPSUControl/RESET.txt

enablePSU SUB
enablePSU PWELL
rampVoltage SUB 15 1.0
rampVoltage PWELL 6.00 1.0

setVoltage DVDD 1.30
enablePSU DVDD

setVoltage LVDD 1.80
enablePSU LVDD

disablePSU AVDD
setVoltage AVDD 1.80

disablePSU IMON2
setVoltage IMON2 1.30



