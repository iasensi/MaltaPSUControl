How to run the PSU control script:

do "MALTA_PSU.py <pathToTaskList>"

A tasklist is just a human readable textfile (see taskList.txt and taskList2.txt as example)
which is parsed by MALTA_PSU.py line by line.
Each line of the task list must first contain the name of one of the methods
available in the MALTA_PSU class (such as i.e. "addPSU") and then all necessary
arguments. All separated by one or more blanks.
For each command a new line must be used.

A task list can also call another task list so that one can compartmentalize.
I.e. a init task list is called by each other task list before anything else
is done.

The available methods include for now only basic commands as given by the TTi and TTiSingle
classes. Keithelys will be implemented next. Then further methods for
monitoring, alerts, etc.
