#!/usr/bin/env python
import SerialCom
import Keithley
import time
import argparse
import sys
import os

parser=argparse.ArgumentParser()
parser.add_argument('-v' ,'--volt',help='set this voltage',type=float,default=0)
parser.add_argument('-s' ,'--set' ,help='set current limit and output ON (once)',action='store_true')
parser.add_argument('-f' ,'--fast',help='minimal amont of operations',action='store_true')
args=parser.parse_args()

voltageLimit=1.75
currentLimit=0.010

if args.volt>voltageLimit:
    print " "
    print " I am sorry ... I am not allowing voltages greater than "+str(voltageLimit)+" V ..."
    print " "
    sys.exit()
if args.volt<0.6 and args.volt!=0:
    print " "
    print " We usually do not allow VPulse below 0.6 V ... are you sure you know what you are doing? "
    print " "
    sys.exit()


keith = Keithley.Keithley("/dev/"+os.environ['KEITH_VPULSE'])#FTZ5U8CXB
if args.set:
    keith.setCurrentLimit(currentLimit)
    keith.setVoltageLimit(voltageLimit)
    keith.enableOutput(True)
    pass

keith.setVoltage(args.volt)

if not args.fast:
    voltage = args.volt ###keith.getVoltage()
    current = keith.getCurrent()
    if voltage>voltageLimit: 
        print " disabled due to exceeding voltage limit: "+str( voltage )
        keith.enableOutput(False)
    elif current>currentLimit: 
        print " disabled due to exceeding current limit: "+str( current )
        keith.enableOutput(False)
    else:
        print " "
        print " VPULSE set to: "+str(voltage)+"  --> with current: "+str( current )
        print " "
        if (args.volt==0): keith.enableOutput(False)
        
keith.close()
