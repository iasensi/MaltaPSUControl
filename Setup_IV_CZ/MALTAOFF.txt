#disablePSU IMON2 

disablePSU LVDD
setVoltage LVDD 0.0

disablePSU DVDD
setVoltage DVDD 0.0

disablePSU AVDD
setVoltage AVDD 0.0

setVoltage PWELL 0.00
setVoltage SUB   0.00 
#disablePSU PWELL
#disablePSU SUB

