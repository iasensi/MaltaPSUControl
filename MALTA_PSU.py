#!/usr/bin/env python
#####################################
#
# MALTA_PSU: A class to handle PSUs
# 
# Florian.Dachs@cern.ch
# Valerio.Dao@cern.ch
# Ignacio.Asensi@cern.ch
# Carlos.Solans@cern.ch
# November 2018
#####################################

import os
import re
import sys
import time
import datetime
import base64
import email.mime.text
import email.mime.multipart
import smtplib

import TTi
import TTiSingle
import Keithley
import HAMEG4040

#define class...
class MALTA_PSU:
    def __init__(self):
        self.PSUNames = []
        self.addedPSUs = dict()
        self.PSUtypes = dict()
        self.PSUchannel = dict()
        self.PSUPort = dict()
        self.PSUVoltageLimit = dict()
        self.PSUCurrentLimit = dict()
        self.PSUVoltageSet = dict()
        self.PSUEnabled = dict()
        self.mailSettings = {"name":"no-reply",
                             "email":"no-reply@cern.ch",
                             "host":"cernmx.cern.ch",
                             "port":25,
                             "user":"",
                             "pass":"",
                             "recipients":[],
                             "auth":False}
        self.cmdLog = ""
        pass

    def parseArgs(self,sysArgs):

        print ("execute tasklist: %s" % os.environ["MALTAPSU_SETUP"]+"/setUpPSU.txt")
        self.executeTaskList(os.environ["MALTAPSU_SETUP"]+"/setUpPSU.txt")

        taskList = ""
        commands = []
        args = []
        cmdArgs = []

        argType = ""
        for i in range(1,len(sysArgs)):
            if sysArgs[i] == "-c":
                if argType == "arg":
                    args.append(cmdArgs)
                    cmdArgs = []
                argType = sysArgs[i]
                continue
            if sysArgs[i] == "-t":
                if argType == "arg":
                    args.append(cmdArgs)
                    cmdArgs = []
                argType = sysArgs[i]
                continue
            if argType == "-t":
                taskList = sysArgs[i]
                continue
            if argType ==  "-c":
                commands.append(sysArgs[i])
                argType = "arg"
                continue
            if argType == "arg":
                cmdArgs.append(sysArgs[i])
                continue
        args.append(cmdArgs)
        #print args
        #print commands
        if taskList != "":
            ####print "ABOUT TO EXECUTE TASKLIST: "+os.environ["MALTAPSU_SETUP"]+"/"+taskList
            self.executeTaskList(os.environ["MALTAPSU_SETUP"]+"/"+taskList)
            pass
        if commands != [] and args != []:
            if len(commands) is not len(args):
                print ("In __init__: command and arg vectors have different size!")
                pass
            for i in range(0,len(commands)):
                self.executeCommand(commands[i],args[i])
                pass
            pass
        
    def addPSU(self, name = None, serialPath = None, deviceType = None, channel = 0, vLimit = 0.0, cLimit = 0.0):
        if name == None:
            name = raw_input("enter channel name (type 'c' to quit):\n")
            if name == "c": sys.exit("script stopped by user")
            pass
        if serialPath == None:
            serialPath = raw_input("enter port path (type 'c' to quit):\n")
            if serialPath == "c": sys.exit("script stopped by user")
            pass
        if deviceType == None:
            deviceType = raw_input("single or multi channel PSU? (s/m; 'c' to quit):\n")
            while deviceType != "s" and deviceType != "m" and deviceType != 'c':
                print("faulty command!")
                deviceType = raw_input("single or multi channel PSU? (s/m; 'c' to quit):\n")
                pass
            pass
        vLimit = float(vLimit)
        cLimit = float(cLimit)
        channel = int(channel)
        if vLimit == 0.0:
            vLimit = raw_input("enter voltage limit:\n")
            pass
        if cLimit == 0.0:
            cLimit = raw_input("enter current limit:\n")
            pass
        com = None
        if deviceType == "c":
            sys.exit("script stopped by user")
            pass
        elif deviceType == "s":
            com = TTiSingle.TTiSingle(serialPath)
            com.setVoltageLimit(vLimit)
            com.setCurrentLimit(cLimit)
            pass
        elif deviceType == "k":
            com = Keithley.Keithley(serialPath)
            com.setVoltageLimit(vLimit)
            com.setCurrentLimit(cLimit)
            pass
        elif deviceType == "m":
            com = TTi.TTi(serialPath)
            if channel == 0:
                channel = raw_input("which channel? (1/2):\n")
                while channel != 1 and channel != 2:
                    print("faulty input!")
                    channel = raw_input("which channel? (1/2):\n")
                    pass
                channel=int(channel)
                pass
            com.setVoltageLimit(channel,vLimit)
            #com.setCurrentLimit(channel,cLimit)
            com.setCurrent(channel,cLimit)
            pass
        elif deviceType == "h3":
            if channel > 3 or channel < 1:
              raise RuntimeError("Invalid channel specified for 3-channel HAMEG (allowed: 1,2,3): {}".format(channel))
            com = HAMEG4040.HAMEG4040(serialPath,9600)
            if channel == 0:
                channel = raw_input("which channel? (1/2/3):\n")
                while channel != 1 and channel != 2 and channel != 3:
                    print("faulty input!")
                    channel = raw_input("which channel? (1/2/3):\n")
                    pass
                channel=int(channel)
                pass
            com.setVoltageLimit(channel,vLimit)
            com.setCurrent(channel,cLimit)
        elif deviceType == "h4":
            if channel > 4 or channel < 1:
              raise RuntimeError("Invalid channel specified for 3-channel HAMEG (allowed: 1,2,3): {}".format(channel))
            com = HAMEG4040.HAMEG4040(serialPath,9600)
            if channel == 0:
                channel = raw_input("which channel? (1/2/3/4):\n")
                while channel != 1 and channel != 2 and channel != 3 and channel != 4:
                    print("faulty input!")
                    channel = raw_input("which channel? (1/2/3/4):\n")
                    pass
                channel=int(channel)
                pass
            com.setVoltageLimit(channel,vLimit)
            com.setCurrent(channel,cLimit)
        else:
            print ("Device not recognized: %s" % deviceType)
            pass
        
        self.PSUNames.append(name)
        self.addedPSUs[name] = com
        self.PSUtypes[name] = deviceType
        self.PSUchannel[name] = int(channel)
        self.PSUPort[name] = serialPath
        self.PSUVoltageLimit[name] = float(vLimit)
        self.PSUCurrentLimit[name] = float(cLimit)
        self.PSUVoltageSet[name] = 0
        self.PSUEnabled[name] = False
        
        pass

    def executeTaskList(self,filepath):
        ##print "loading filepath:", filepath
        command = None
        commandArgs = []
        with open(filepath) as f:
            for line in f:
                if line[0] != '#':
                    #the following regex first checks if there are quotation marks
                    #if yes, match everything between the quotation marks, if no, match char-arrays which are delimited by whitespace
                    regex = r'((?<=\")[\w\s]+?(?=\"))|((?!\")[^\s]+)'
                    args = re.findall(regex,line)
                    for i in range(0,len(args)):
                        #regex returns a list of pairs where the first entry is only filled if the args was in quotation marks
                        #and the second entry is only filled if the entry was not, this is mutually exclusive and can be flattened
                        #by joining the pairs
                        args[i] = "".join(args[i])
                    if len(args) > 0:
                        command = args[0]
                        commandArgs = []
                        for i in range(1,len(args)):
                            commandArgs.append(args[i])
                        self.executeCommand(command,commandArgs)
                    else:
                        pass
                        #print "In executeTaskList: No arguments given, emtpy line?"
                else:
                    if line[0] == '#':
                        pass
                    else:
                        print ("In executeTaskList: Could not parse line: \n {}".format(line))
        return 0

    def executeCommand(self,command,args):
        if command == 'getVoltage' and len(args) == 1:
            print (args[0], self.getVoltage(args[0]))
            pass
        elif command == 'getVoltageLimit' and len(args) == 1:
            print (args[0], self.getVoltageLimit(args[0]))
            pass
        elif command == 'getCurrent' and len(args) == 1:
            print (args[0], self.getCurrent(args[0]))
            pass
        elif command == 'getCurrentLimit' and len(args) == 1:
            print (args[0], self.getCurrentLimit(args[0]))
            pass
        elif command == 'setVoltage' and len(args) == 2:
            self.setVoltage(args[0],args[1])
            pass
        elif command == 'setVoltageLimit' and len(args) == 2:
            self.setVoltageLimit(args[0],args[1])
            pass
        elif command == 'setCurrentLimit' and len(args) == 2:
            self.setCurrentLimit(args[0],args[1])
            pass
        elif command == 'rampVoltage' and len(args) > 2:
            PSUs = []
            values = []
            for i in range(0,int(len(args)/2)): PSUs.append(args[i])
            for i in range(int(len(args)/2),len(args)-1): values.append(float(args[i]))
            timeWindow = float(args[-1])
            self.rampVoltage(PSUs[0],values[0],timeWindow)
            pass
        elif command == 'rampVoltageUp' and len(args) > 2:
            PSUs = []
            values = []
            for i in range(0,int(len(args)/2)): PSUs.append(args[i])
            for i in range(int(len(args)/2),len(args)-1): values.append(float(args[i]))
            timeWindow = float(args[-1])
            self.rampVoltageUp(PSUs[0],values[0],timeWindow)
            pass
        elif command == 'rampVoltageDown' and len(args) > 2:
            PSUs = []
            values = []
            for i in range(0,int(len(args)/2)): PSUs.append(args[i])
            for i in range(int(len(args)/2),len(args)-1): values.append(float(args[i]))
            timeWindow = float(args[-1])
            self.rampVoltageDown(PSUs[0],values[0],timeWindow)
            pass
        elif command == 'showConnectedPSUs' and len(args) == 0:
            self.showConnectedPSUs()
            pass
        elif command == 'addPSU' and len(args) == 6:
            self.addPSU(args[0],args[1],args[2],args[3],args[4],args[5])
            pass
        elif command == 'executeTaskList' and len(args) == 1:
            self.executeTaskList(args[0])
            pass
        elif command == 'enablePSU' and len(args) > 0:
            self.enablePSU(args[0])
            pass
        elif command == 'disablePSU' and len(args) > 0:
            self.disablePSU(args[0])
            pass
        elif command == 'sendMail' and len(args) > 1:
            files = []
            for i in range(2,len(args)): files.append(args[i])
            self.sendMail(args[0],args[1],files)
            pass
        elif command == 'setupMail' and len(args) > 2:
            recipients = []
            for i in range(2,len(args)): recipients.append(args[i])
            self.setupMail(args[0],args[1],recipients)
            pass
        elif command == 'setCurrentRange' and len(args) > 1:
            self.setCurrentRange(args[0],args[1])
            pass
        elif command == 'setCurrentHighPrecision' and len(args) > 0:
            self.setCurrentHighPrecision(args[0])
            pass
        elif command == 'monitorValues' and len(args) > 2:
            if args[0] == 'all':
                alertAction = args[1]
                complianceLimit = int(args[2])
                PSUs = []
                values = []
                for i in range(0,len(self.PSUNames)):
                    values.append("voltage")
                    values.append("current")
                    PSUs.append(self.PSUNames[i])
                    PSUs.append(self.PSUNames[i])
                    pass
                self.monitorValues(PSUs,values,alertAction,complianceLimit)
                return 0
            else:
                PSUs = []
                values = []
                for i in range(0,int(len(args)/2)-1): PSUs.append(args[i])
                for i in range(int(len(args)/2)-1,len(args)-2): values.append(args[i])
                alertAction = args[-2]
                complianceLimit = int(args[-1])
                self.monitorValues(PSUs,values,alertAction,complianceLimit)
                return 0
            pass
        elif command == 'sleep' and len(args) == 1:
            self.sleep(args[0])
            pass
        else:
            print ("Could not parse command, this was read in:\n")
            print ("Command: {}\n".format(command))
            print ("Arguments: \n")
            for i in range(0,len(args)):
                print ("    {}\n".format(args[i]))
                pass
            return -1
        return 0

    def sleep(self,timeWindow):
        msg="Sleeping for {} seconds...".format(timeWindow)
        self.log(msg)
        time.sleep(float(timeWindow))
        pass

    def log(self,msg):
        print (msg)
        self.cmdLog+=msg+"\n"
        if(len(self.cmdLog)>5000): self.cmdLog=self.cmdLog[:-5000]
        pass

    def clearCmdLog(self):
        self.cmdLog=""
        pass
    
    def getCmdLog(self):
        return self.cmdLog[:-1]

    def enablePSU(self,PSUName):
        if PSUName in self.PSUNames:
            value = self.isEnabled(PSUName)
            if value:
                self.PSUEnabled[PSUName]=value
                print( PSUName, "already enabled, skip", value)
                return value
            self.PSUEnabled[PSUName]=True
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's':
                com.enableOutput(True)
                #self.log("Enabled {}".format(PSUName))
                return 1
            elif devType == 'k':
                com.enableOutput(True)
                #self.log("Enabled {}".format(PSUName))
                #voltage=com.getVoltage()
                #print " returned Voltage is: "+str(voltage)
                return 1
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                com.enableOutput(self.PSUchannel[PSUName],True)
                #self.log("Enabled {}".format(PSUName))
                return 1
            pass
        else:
            self.log("In enablePSU: Could not find {} among added PSUs!".format(PSUName))
            return -1
            pass
        pass
    
    def disablePSU(self,PSUName):
        if PSUName in self.PSUNames:
            value = self.isEnabled(PSUName)
            if value==0:
                self.PSUEnabled[PSUName]=value
                print (PSUName, "already disabled, skip")#, value
                return value
            self.PSUEnabled[PSUName]=False ##why was it used to be True in the disablePSU function?
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                com.enableOutput(False)
                #self.log("Enabled {}".format(PSUName))
                return 1
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                com.enableOutput(self.PSUchannel[PSUName],False)
                #self.log("Enabled {}".format(PSUName))
                return 1
            pass
        else:
            self.log("In disablePSU: Could not find {} among added PSUs!".format(PSUName))
            return -1
            pass
        pass
    
    def getModel(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            if devType == 's' or devType == 'k' or devType == 'm' or devType == 'h3' or devType == 'h4':
                return com.getModel()
            pass
        else:
            self.log("In getModel: Cannot find {} among added PSUs!".format(PSUName))
            return ""
        return 0

    def getVoltage(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                voltage = com.getVoltage()
                return float(voltage)
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                voltage = com.getVoltage(channel)
                #print ("Valerio: "+str(voltage))
                return float(voltage)
            pass
        else:
            self.log("In getVoltage: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def getCurrent(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                current = com.getCurrent()
                return float(current)
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                current = com.getCurrent(channel)
                return float(current)
            pass
        else:
            self.log("In getCurrent: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def setCurrentRange(self,PSUName,value):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                com.setCurrentRange(value)
                return 0
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                print ("No setCurrentRange for this PSU")
                return 0
        else:
            self.log("In getCurrent: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def setCurrentHighPrecision(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                com.setCurrentHighPrecision()
                return 0
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                print ("No setCurrentHighPrecision for this PSU")
                return 0
        else:
            self.log("In getCurrent: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def setCurrentRange(self,PSUName,value):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                com.setCurrentRange(value)
                return 0
            elif devType == 'm':
                print ("No setCurrentRange for TTi PSUs")
                return 0
        else:
            self.log("In getCurrent: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def setCurrentHighPrecision(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                com.setCurrentHighPrecision()
                return 0
            elif devType == 'm':
                print( "No setCurrentRange for TTi PSUs")
                return 0
        else:
            self.log("In getCurrent: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def setVoltage(self,PSUName,value):
        self.PSUVoltageSet[PSUName]=float(value)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            vLimit = self.PSUVoltageLimit[PSUName]
            if abs(float(value)) > abs(float(vLimit)):
                self.log("{} {} exceeds voltage limit: {} V".format(PSUName,value,vLimit))
                return -1
            if devType == 's' or devType == 'k':
                com.setVoltage(float(value))
                pass
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                com.setVoltage(int(channel),float(value))
                pass
            self.log("Voltage of {} set to {:.3f}".format(PSUName,float(value)))
        else:
            self.log("In setVoltage: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def setVoltageLimit(self,PSUName,value):
        self.PSUVoltageLimit[PSUName]=float(value)
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                com.setVoltageLimit(float(value))
                pass
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                com.setVoltageLimit(int(channel),float(value))
                pass
            self.log("Voltage limit for {} set to {:.3f}".format(PSUName,float(value)))
        else:
            self.log("In setVoltageLimit: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def getVoltageSet(self,PSUName):
        if PSUName in self.PSUNames:
            return self.PSUVoltageSet[PSUName]
        else:
            print ("In getVoltageSet: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def getSetVoltage(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                val = round(float(com.getSetVoltage()),2)
                self.log("Voltage set for {}: {}V".format(PSUName,val))
                return val
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                print ("RAW VOLTAGE SET:", com.getSetVoltage(channel))
                val = round(float(com.getSetVoltage(channel)),2)
                self.log("Voltage set for {}: {}V".format(PSUName,val))
                return val
            pass
        else:
            self.log("In getSetVoltage: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def getVoltageLimit(self,PSUName):
        if PSUName in self.PSUNames:
            return self.PSUVoltageLimit[PSUName]
        else:
            print ("In getVoltageLimit: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def readVoltageLimit(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                val = round(float(com.getVoltageLimit()[0:-1]),2)
                self.log("Voltage limit for {}: {}V".format(PSUName,val))
                return val
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                val = round(float(com.getVoltageLimit(channel)[0:-1]),2)
                self.log("Voltage limit for {}: {}V".format(PSUName,val))
                return val
            pass
        else:
            self.log("In readVoltageLimit: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def getCurrentLimit(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            return self.PSUCurrentLimit[PSUName]
        else:
            self.log("In getCurrentLimit: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def readCurrentLimit(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                val = round(float(com.getCurrentLimit()[0:-1]),2)
                self.log("Current limit for {}: {}A".format(PSUName,val))
                return val
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                val = round(float(com.getCurrentLimit(channel)[3:]),2)
                self.log("Current limit for {}: {}A".format(PSUName,val))
                return val
        else:
            self.log("In getCurrentLimit: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def setCurrentLimit(self,PSUName,value):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                com.setCurrent(float(value))
                pass
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                com.setCurrent(channel,float(value))
                pass
            self.log("Current limit for {} set to {:.3f}".format(PSUName,float(value)))
        else:
            self.log("In setCurrentLimit: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0

    def isEnabled(self,PSUName):
        if PSUName in self.PSUNames:
            com = self.addedPSUs[PSUName]
            devType = self.PSUtypes[PSUName]
            channel = self.PSUchannel[PSUName]
            if devType == 's' or devType == 'k':
                value=com.isEnabled()
            elif devType == 'k':
                value=com.isEnabled()
                #voltage=com.getVoltage()
                #print " returned Voltage is: "+str(voltage)
            elif devType == 'm' or devType == 'h3' or devType == 'h4':
                value=com.isEnabled(channel)
            self.PSUEnabled[PSUName]=value
            self.log("Output for {} enabled {}".format(PSUName,value))
            return value
        else:
            self.log("In isEnabled: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        return 0
    
    def rampVoltage(self,PSUName,val,tW=1):
        if PSUName in self.PSUNames:
            pass
        else:
            self.log("In rampVoltage: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        newVoltage = float(val)
        timeWindow = float(tW)
        nSteps = int(timeWindow/0.2)
        voltageStep = 0
        if nSteps == 0:
            nSteps = 1
            pass
        vLimit = self.PSUVoltageLimit[PSUName]
        if abs(newVoltage) > abs(vLimit):
            self.log("In rampVoltage: Voltage exceeds voltage limit!")
            return -1
        oldVoltage = round(self.getVoltage(PSUName),2)
        #if oldVoltage < -0.01: oldVoltage=0.0
        voltageChange = newVoltage-oldVoltage
        voltageStep = voltageChange/nSteps
        if round(voltageChange,2) == 0.0:
            self.log("{} already set to {:.3f}, no ramp needed".format(PSUName,newVoltage))
            return 0
        for i in range(1,nSteps+1):
            self.setVoltage(PSUName,oldVoltage+voltageStep*i)
            #some sleep is necessary to let the PSU stabilize
            time.sleep(0.2)
            pass
        time.sleep(0.2)
        newVoltage = self.getVoltage(PSUName)
        self.log("Voltage of {} ramped from {:.3f} to {:.3f}".format(PSUName,oldVoltage,newVoltage))
        return 1
        
    def rampVoltageUp(self,PSUName,val,tW=1):
        if PSUName in self.PSUNames:
            pass
        else:
            self.log("In rampVoltage: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        newVoltage = float(val)
        timeWindow = float(tW)
        nSteps = int(timeWindow/0.2)
        voltageStep = 0
        if nSteps == 0:
            nSteps = 1
            pass
        vLimit = self.PSUVoltageLimit[PSUName]
        if abs(newVoltage) > abs(vLimit):
            self.log("In rampVoltage: Voltage exceeds voltage limit!")
            return -1
        oldVoltage = round(self.getVoltage(PSUName),2)
        if oldVoltage >= newVoltage:
            print ("Old voltage", oldVoltage, "is >= target", newVoltage, ", skipping this step")
            return 0
        #if oldVoltage < -0.01: oldVoltage=0.0
        voltageChange = newVoltage-oldVoltage
        voltageStep = voltageChange/nSteps
        if round(voltageChange,2) == 0.0:
            self.log("{} already set to {:.3f}, no ramp needed".format(PSUName,newVoltage))
            return 0
        for i in range(1,nSteps+1):
            self.setVoltage(PSUName,oldVoltage+voltageStep*i)
            #some sleep is necessary to let the PSU stabilize
            time.sleep(0.2)
            pass
        time.sleep(0.2)
        newVoltage = self.getVoltage(PSUName)
        self.log("Voltage of {} ramped from {:.3f} to {:.3f}".format(PSUName,oldVoltage,newVoltage))
        return 1
        
    def rampVoltageDown(self,PSUName,val,tW=1):
        if PSUName in self.PSUNames:
            pass
        else:
            self.log("In rampVoltage: Cannot find {} among added PSUs!".format(PSUName))
            return -1
        newVoltage = float(val)
        timeWindow = float(tW)
        nSteps = int(timeWindow/0.2)
        voltageStep = 0
        if nSteps == 0:
            nSteps = 1
            pass
        vLimit = self.PSUVoltageLimit[PSUName]
        if abs(newVoltage) > abs(vLimit):
            self.log("In rampVoltage: Voltage exceeds voltage limit!")
            return -1
        oldVoltage = round(self.getVoltage(PSUName),2)
        if oldVoltage <= newVoltage:
            print ("Old voltage", oldVoltage, "is <= target", newVoltage, ", skipping this step")
            return 0
        #if oldVoltage < -0.01: oldVoltage=0.0
        voltageChange = newVoltage-oldVoltage
        voltageStep = voltageChange/nSteps
        if round(voltageChange,2) == 0.0:
            self.log("{} already set to {:.3f}, no ramp needed".format(PSUName,newVoltage))
            return 0
        for i in range(1,nSteps+1):
            self.setVoltage(PSUName,oldVoltage+voltageStep*i)
            #some sleep is necessary to let the PSU stabilize
            time.sleep(0.2)
            pass
        time.sleep(0.2)
        newVoltage = self.getVoltage(PSUName)
        self.log("Voltage of {} ramped from {:.3f} to {:.3f}".format(PSUName,oldVoltage,newVoltage))
        return 1

    def setupMail(self,sender,senderPW,recipients):
        #ignore sender and password
        self.mailSettings["recipients"] = recipients
        pass
    
    def sendMail(self,subject,message,files=None):
        if len(self.mailSettings["recipients"])==0:
            print ("Mail contacts were not set up!")
            return -1
        smtp = smtplib.SMTP(self.mailSettings["host"],self.mailSettings["port"])
        smtp.ehlo()
        msg = email.mime.multipart.MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = self.mailSettings["name"]
        text = email.mime.text.MIMEText(message)
        msg.attach(text)
        for recipient in self.mailSettings["recipients"]:
            msg['To'] = recipient
            smtp.sendmail(self.mailSettings["email"],recipient,msg.as_string())
            print ("alert mail was sent to {}\n".format(recipient))
            pass
        pass

    def monitorValues(self,PSUs,values,alertAction,complianceLimit):
        if len(PSUs) != len(values):
            print ("In monitorValues: Argument lists have different lengths!")
            return -1
        for i in range(0,len(PSUs)):
            if PSUs[i] not in self.PSUNames:
                print ("In monitorValue: Cannot find {} among added PSUs!".format(PSUs[i]))
                return -1
            else:
                pass
        for i in range(0,len(values)):
            if values[i] != "voltage" and values[i] != "current":
                print ("In monitorValue: Cannot monitor {}, value unknown!".format(values[i]))
                return -1
            else:
                pass
        #open file
        #write header line with columns for monitored PSUs
        #go through PSUs and read out the corresponding values
        #concatenate string of read values and time stamp and write to file
        #check if any value is in compliance, if yes, send alert and end monitoring if stopIfAlert=True
        if not os.path.exists("monitoring"):
            try:
                os.makedirs("monitoring")
            except OSError as e:
                if e.errno != errno.EEXIST:
                    pass
                else:
                    print ("In monitorValues: Could not create monitoring Folder!")
                    return -1
        else:
            pass

        if self.mailSender == None:
            print ("The mail contacts do not seem to be set up!")
            print ("Please do this before starting monitoring.")
            return -1

        dateandtime = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
        fileName = "monitoring/PSUmonitoring_" + dateandtime + ".txt"
        fileTwinCounter = 0
        f = None
        fileOpenFail = 0
        while(fileOpenFail < 10):
            try:
                f = open(fileName,"w")
                break;
            except:
                fileTwinCounter = fileTwinCounter+1
                fileName = "monitoring/PSUmonitoring_" + dateandtime + "_" + string(fileTwinCounter) + ".txt"
                fileOpenFail += 1

        columnWidths = []
        dateandtime2 = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        columnWidths.append(len(dateandtime2)+3)
        headerLine = "{col:<{width}}".format(width=len(dateandtime2)+3,col="#Time:")
        for i in range(0,len(PSUs)):
            entry = PSUs[i]+"("+values[i][0:1]+"):"
            columnWidths.append(len(entry)+3)
            headerLine = headerLine + "{col:<{width}}".format(width=len(entry)+3,col=entry)
        f.write(headerLine+"\n")
        complianceCounter = 0
        sentAlert = False
        keepMonitoring = True
        try:
            print ("Monitoring values, press Ctrl-C to stop...")
            print (headerLine)
            headerLinePrint = 0
            while keepMonitoring:
                inCompliance = False
                dateandtime2 = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
                newLine = "{col:<{width}}".format(width=columnWidths[0],col=dateandtime2)
                curVal = 0.0
                for i in range(0,len(PSUs)):
                    #read in val from PSUs
                    if values[i] == "voltage":
                        curVal = self.getVoltage(PSUs[i])
                        if curVal == -2:
                            keepMonitoring = False
                            break
                        if curVal == -1:
                            print ("Could not read out {} from {}!".format(values[i],PSUs[i]))
                            keepMonitoring = False
                            break
                        if abs(curVal) >= self.PSUVoltageLimit[PSUs[i]]:
                            inCompliance = True
                    elif values[i] == "current":
                        curVal = self.getCurrent(PSUs[i])
                        if curVal == -2:
                            keepMonitoring = False
                            break
                        if curVal == -1:
                            print ("Could not read out {} from {}!".format(values[i],PSUs[i]))
                            keepMonitoring = False
                            break
                        if abs(curVal) >= self.PSUCurrentLimit[PSUs[i]]:
                            inCompliance = True
                    #add val to newline
                    newLine = newLine + "{col:<{width}}".format(width=columnWidths[i+1],col=curVal)
                #write newLine to file
                if not keepMonitoring:
                    break
                f.write(newLine+"\n")
                headerLinePrint += 1
                if headerLinePrint == 20:
                    print (headerLine)
                    headerLinePrint = 0
                print (newLine)
                newLine = ""
                #if limit reached, break
                if inCompliance and not sentAlert:
                    #send alert mail
                    complianceCounter += 1
                    print ("ComplianceCounter: {}".format(complianceCounter))
                    if complianceCounter > complianceLimit:
                        subject = "MALTA_PSU: Monitored value in compliance!"
                        message = ""
                        files = []
                        files.append(fileName)
                        f.close()
                        #self.sendMail(subject,message,files)
                        sentAlert = False#True
                        f = open(fileName,"a")
                        if alertAction != "None":
                            self.executeTaskList(alertAction)
                            break
                    else:
                        pass
                else:
                    complianceCounter = 0
                    pass
            print ("\n")
            print ("Monitoring stopped, closing file...")
            f.close()
            print ("...done, good bye.")
        except KeyboardInterrupt:
            print ("\n")
            print ("Monitoring stopped, closing file...")
            f.close()
            print ("...done, good bye.")
        f.close()
        return 0

    def showConnectedPSUs(self):
        connected_psus={}
        for i in range(0,len(self.PSUNames)):
            name = self.PSUNames[i]
            channel = self.PSUchannel[name]
            port = self.PSUPort[name]
            devType = self.PSUtypes[name]
            com = self.addedPSUs[name]
            model = com.getModel() 
            print ("PSU {} type {} port {} channel {}: {}".format(name,devType,channel,port,model))
            connected_psus[name]={"channel":channel, "port":port}
        if len(self.PSUNames) == 0:
            print ("No PSUs seem to be connected!")
        return connected_psus

#class is defined, now execute tasks
if __name__=="__main__":
    psu = MALTA_PSU()
    psu.parseArgs(sys.argv)
