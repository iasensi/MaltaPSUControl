disablePSU IMON2 

disablePSU REST
setVoltage REST 0.0

disablePSU DVDD
setVoltage DVDD 0.0
rampVoltage SUB   0.00 1.0
enablePSU SUB

disablePSU REST2
setVoltage REST2 0.0

disablePSU DVDD2
setVoltage DVDD2 0.0
rampVoltage SUB2   0.00 1.0
enablePSU SUB2


